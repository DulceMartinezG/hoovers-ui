module.exports = {
  stories: ['../src/**/*.stories.@(ts|tsx|js|jsx|mdx)'],
  addons: [
    {
      name: '@storybook/addon-docs',
      options: {
        configureJSX: true
      }
    },
    '@storybook/addon-actions',
    '@storybook/addon-controls',
    '@storybook/addon-knobs',
    '@storybook/addon-links',
    '@storybook/preset-create-react-app',
    'storybook-addon-deps/preset'
  ]
}
