/**
 * class ButtonExample
 **/

import * as React from 'react'
import './buttonExample.scss'

interface IButtonExampleProps {
	name: string
	testId: string
	onClick(): void
}

export const ButtonExample: React.FC<IButtonExampleProps> = ({ name, testId, onClick }: IButtonExampleProps) => {
	return (
		<div className="myButton" id={testId} onClick={() => onClick()}>
			<span>{name}</span>
		</div>
	)
}

export default ButtonExample
